#!/bin/bash
# Script to check that mysqlrouter_exporter connects to mysqlrouter and
# provides metrics after a basic configuration.

set -ex

# Create a basic mysqlrouter config file that connects to local mysql-server
create_mysqlrouter_config_file() {
    mkdir -p /etc/mysqlrouter/
    cat <<EOF > /etc/mysqlrouter/mysqlrouter.conf
[DEFAULT]
logging_folder = /var/log/mysqlrouter

[logger]
level = INFO

[routing:main]
bind_address = 127.0.0.1
bind_port = 3310
destinations=127.0.0.1:3306
mode = read-write
routing_strategy = round-robin

[keepalive]
interval = 60

[http_server]
port=8080

[rest_api]

EOF
}

# Create systemd service file for mysqlrouter
create_mysqlrouter_service_file() {
    cat <<EOF > /etc/systemd/system/mysqlrouter.service
[Unit]
Description=MySQL Router
After=network.target

[Service]
Type=exec
ExecStart=mysqlrouter
RemainAfterExit=yes
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
}

# Create systemd service file for mysqlrouter-exporter
create_mysqlrouter_exporter_service_file() {
    cat <<EOF > /etc/systemd/system/mysqlrouter-exporter.service
[Unit]
Description=mysqlrouter-exporter
Documentation=https://github.com/rluisr/mysqlrouter-exporter
After=network.target

[Service]
Type=simple
Environment="MYSQLROUTER_EXPORTER_URL=http://127.0.0.1:8080"
Environment="MYSQLROUTER_EXPORTER_SERVICE_NAME=mysqlrouter"
ExecStart=/usr/bin/mysqlrouter_exporter

[Install]
WantedBy=multi-user.target

EOF
}

# Start mysql router and mysqlrouter exporter
start_routing() {
    systemctl start mysqlrouter
    systemctl start mysqlrouter-exporter
    sleep 60 # Avoid race condition with Type=simple service startup
}

create_mysqlrouter_config_file
create_mysqlrouter_service_file
create_mysqlrouter_exporter_service_file

start_routing

curl -s http://localhost:9152/metrics
METRICS_FOUND=$?

if [ "$METRICS_FOUND" != 0 ]; then
    echo "Error: mysqlrouter_exporter metrics not found."
    exit 1
fi
